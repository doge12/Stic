/*
  Name: Player Class
  Copyright: Sam Haftka
  Author: Sam Haftka
  Date Created: 12/10/12 14:34
  Last updated: 28/04/2014 13:58
  Description: A class that is used to represent the players and their 
               interaction with the rest of the game/program.
*/


#ifndef Player_h
    #define Player_h

#include "Character.h"
#include "CEntity.h"
#include <ctime>
#include <string>
using namespace std;

class Player
{
    private:
        Character *playerCharacter;
        CEntity playerEntity;
        CEntity healthEntity;
        
        int health;
        string file;
        
        //Physics variables:
        bool move;
        int direction;
        int ground;
        bool dirSwitch;
        bool isFalling;
        
        //Clock tick to fix movement speed
        clock_t oldTime;
    
    public:
        Player();
        
        Player( Character *choice);
        
        void selectCharacter( Character *choice );
        
        int drawCharacter(int X, bool side);
        
        void stand();
        
        void kick();
        
        void punch();
        
        void walk(int d);
        
        void movement();
        
        void falling();
        
        void jump(int max);
        
        void stop();
        
        void turn(bool side);
        
        bool takeDamage(int damage);
        
        int doDamage();
        
        void pushback();
        
        int getHealth();
        
        void setHealth(int h);
        
        int getX();
        
        void OnCleanup();
        
        ~Player();
};

#endif
