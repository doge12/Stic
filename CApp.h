#ifndef _CAPP_H_
    #define _CAPP_H_

#include <SDL/SDL.h>


#include "CAnimation.h"
#include "CEvent.h"
#include "CSurface.h"
#include "CEntity.h"
#include "Player.h"


class CApp : public CEvent {
      private:
        bool Running;
        SDL_Surface* Surf_Display;
        
        SDL_Surface* Surf_Background;
        
        //create player One and Two as well
        //as characters Blue and Red.
        Player          playerOne;
        Player          playerTwo;
        Character       *Blue;
        Character       *Red;

      public:
        CApp();
        int OnExecute();

      public:
        bool OnInit();
        void OnEvent(SDL_Event* Event);
        void OnExit(); 
        void OnLoop();
        void OnRender();
        void OnCleanup();
        void OnKeyDown(SDLKey sym, SDLMod mod, Uint16 unicode);
        void OnKeyUp(SDLKey sym, SDLMod mod, Uint16 unicode);
        
};

#endif
