#ifndef CEntity_h
    #define CEntity_h

#include <vector>

#include "CAnimation.h"
#include "CSurface.h"

class CEntity{
   public:
      static std::vector<CEntity*>  EntityList;
         
   protected:
      CAnimation  Anim_Control;
         
      SDL_Surface*   Surf_Entity;
         
   public:
      int X;
      int Y;
      
      int Width;
      int Height;
      
      int AnimState;
      
   public:
      CEntity();
      
      virtual ~CEntity();
      
   public:
      virtual bool OnLoad(char* file, int Width, int Height, int MaxFrames, 
                                int x, int y);
      
      //added for an simpler/faster draw/image change method
      virtual bool OnChange(char* file, int MaxFrames);
      
      virtual void OnLoop();
      
      virtual void OnRender(SDL_Surface* Surf_Display);
      
      virtual void OnCleanup();
      
      virtual void push();
      
      //added for movement purposes
      CEntity& operator++();
      //added for movement purposes
      CEntity& operator--();
      //added for movement purposes
      CEntity& operator+(int num);
      //added for movement purposes
      CEntity& operator-(int num);
      //added for physics calculation.
      int getY();
      //added for damage/battle calculations.
      int getX();
   
};
#endif
