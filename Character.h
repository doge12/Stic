/*
  Name: Character Class
  Copyright: Sam Haftka
  Author: Sam Haftka
  Date Created: 12/10/12 14:34
  Last Updated: 4/03/13 12:30
  Description: A class that is used to represent the the fifferent characters 
               that players can choose/play with and how the effect the player
               class.  
*/

#ifndef Character_h
    #define Character_h
    
#include <string>
using namespace std;

class Character
{
    private:
        
        int power;
        
        //variables that are not yet implemented with the game.
        //but will in future verisons.
        int speed;
        int comboDamage;
        int comboSet;
        
        string name;
    
    public:
        Character();
        
        Character(string n);
        
        Character( int s, int p, int cB, int cS);
        
        void setValues ( int s, int p, int cB, int cS);
        
        int getPower();
        
        string getName();
        
        ~Character();
};

#endif
