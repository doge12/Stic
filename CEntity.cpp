#include "CEntity.h"
 
std::vector<CEntity*> CEntity::EntityList;
 
CEntity::CEntity() {
    Surf_Entity = NULL;
 
    X = Y = 0;
 
    Width = Height = 0;
 
    AnimState = 0;
}
 
CEntity::~CEntity() {
}
 
bool CEntity::OnLoad(char* File, int Width, int Height, int MaxFrames,
                           int x, int y) {
    if((Surf_Entity = CSurface::OnLoad(File)) == NULL) {
        return false;
    }
 
    CSurface::Transparent(Surf_Entity, 255, 255, 255);
    
    this->Width = Width;
    this->Height = Height;
    this->X = x;
    this->Y = y;
 
    Anim_Control.MaxFrames = MaxFrames;
 
    return true;
}

//Added for an simpler/faster draw/image change method
bool CEntity::OnChange(char* File, int MaxFrames)
{
    if((Surf_Entity = CSurface::OnLoad(File)) == NULL) {
        return false;
    }
    
    CSurface::Transparent(Surf_Entity, 255, 255, 255);
    Anim_Control.MaxFrames = MaxFrames;
 
    return true;
}
 
void CEntity::OnLoop() {
    Anim_Control.OnAnimate();
}
 
void CEntity::OnRender(SDL_Surface* Surf_Display) {
    if(Surf_Entity == NULL || Surf_Display == NULL) return;
 
    CSurface::OnDraw(Surf_Display, Surf_Entity, X, Y, AnimState * Width, 
                                   Anim_Control.GetCurrentFrame() * Height, 
                                   Width, Height);
}
 
void CEntity::OnCleanup() {
  //  if(!Surf_Entity == NULL) {    //part of original code, now causes build errors?
        SDL_FreeSurface(Surf_Entity);
   // }
 
    Surf_Entity = NULL;
}

void CEntity::push()
{
    CEntity::EntityList.push_back(this);
    
}

//added operator overloads:
CEntity& CEntity::operator++()
{
    X += 1;    
}

CEntity& CEntity::operator--()
{
    X-=1;
}

CEntity& CEntity::operator+(int num)
{
    Y -= num;    
}

CEntity& CEntity::operator-(int num)
{
    Y += num;
}

int CEntity::getY()
{
    return Y;    
}

int CEntity::getX()
{
    return X;    
}

