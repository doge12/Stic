#include "CApp.h"

void CApp::OnCleanup() {
     SDL_FreeSurface(Surf_Display);
     SDL_FreeSurface(Surf_Background);
     
     
     for(int i = 0;i < CEntity::EntityList.size();i++) 
     {
     if(CEntity::EntityList[i] == NULL) continue;
 
     CEntity::EntityList[i]->OnCleanup();
     }
     
     //deletes the characters.
     delete Red;
     delete Blue;
     
     //deletes the players.
     playerOne.OnCleanup();
     playerTwo.OnCleanup();
 
     CEntity::EntityList.clear();
     SDL_Quit();
}
