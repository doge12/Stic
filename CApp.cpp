#include "CApp.h"

CApp::CApp() {
    Surf_Display = NULL;
    Surf_Background = NULL;
    
    Running = true;
    
    //assign characters their names
    Blue = new Character("Blue");
    Red = new Character("Red");
    //assign players their characters.
    playerOne.selectCharacter(Blue);
    playerTwo.selectCharacter(Red);

}

int CApp::OnExecute() {
    if(OnInit() == false) {
        return -1;
    }

    SDL_Event Event;
    bool start = true;
    while(Running) {
        while(SDL_PollEvent(&Event)) {
            OnEvent(&Event);
        }

        OnLoop();
        OnRender();
    }

    OnCleanup();

    return 0;
}

int main(int argc, char* argv[]) {
    CApp theApp;

    return theApp.OnExecute();
}
