#include "CApp.h"

bool CApp::OnInit() {
    if(SDL_Init(SDL_INIT_EVERYTHING) < 0) {
        return false;
    }
    
    //creates app screen.
    if((Surf_Display = SDL_SetVideoMode(640, 380, 32, 
                     SDL_HWSURFACE | SDL_DOUBLEBUF)) == NULL) {
        return false;
    }
    
    SDL_WM_SetCaption("Stick Fighters Alpha 0.0.3", "");
    
    //Surf_Background = CSurface::OnLoad("nenen.bmp");
    if((Surf_Background = CSurface::OnLoad("Background.bmp")) == NULL) {
        return false;
    }
    
    //draws player one to the screen.
    if(playerOne.drawCharacter(0, true) == false){
        return false;
    }
    
    //draws player two to the screen
    if(playerTwo.drawCharacter(340, false) == false){
        return false;
    }
    
    
    //pops them in the entity list.
    playerOne.pushback();   
    playerTwo.pushback();
    

    return true;
}
