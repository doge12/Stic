#include "CApp.h"

void CApp::OnLoop() {
    
    //phyics loops:
    playerOne.movement();
    playerOne.falling();
    
    playerTwo.movement();
    playerTwo.falling();
    
    //turn loops.
    if(playerOne.getX() < playerTwo.getX())
    {
        playerOne.turn(true);
        playerTwo.turn(false);
    }
    else
    {
        playerOne.turn(false);
        playerTwo.turn(true);
    }
   
   for(int i = 0;i < CEntity::EntityList.size();i++) 
   {
    if(!CEntity::EntityList[i]) continue;
 
    CEntity::EntityList[i]->OnLoop();
   }
   
}
