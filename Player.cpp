#include "Player.h"

Player::Player()
{
    playerCharacter = new Character;
    direction = 0;
    move = false;
    ground = 100;
    health = 100;
    file = "default";
    dirSwitch = false;
    oldTime = clock();
    isFalling = false;
}

Player::Player( Character *choice)
{
    playerCharacter = choice;
    file = playerCharacter->getName();     
}

//selects the character.
void Player::selectCharacter( Character *choice)
{
    playerCharacter = choice;
    file = playerCharacter->getName();    
}

//called when the player is first drawn on the screen/when game resets
//also sets up which direction he starts off facing.
int Player::drawCharacter(int X, bool side)
{
    dirSwitch = side;
    if(healthEntity.OnLoad((char*)(file + "\\100.bmp").c_str(), 300, 20, 1, X ,
                                        10) == false) {
            return false;
    }

        
    if(dirSwitch)
    {
        if(playerEntity.OnLoad((char*)(file + "\\StickFighter1.bmp").c_str(), 
                                            150, 200, 4, X ,100) == false) {
            return false;
        }
    }
    else
    {
        if(playerEntity.OnLoad((char*)(file + "\\StickFighter2.bmp").c_str(), 
                                            150, 200, 4, X ,100) == false) {
           return false;
        }
    }
 
    return true;
}

//checks if player is moving or not and assigns the correct animation.
void Player::stand()
{
    if(!move)
    {
        if(dirSwitch)
            playerEntity.OnChange((char*)(file + "\\StickFighter1.bmp")
                                               .c_str(), 4);
        else
            playerEntity.OnChange((char*)(file + "\\StickFighter2.bmp")
                                               .c_str(), 4);
    }
    else
    {
        if(dirSwitch)
            playerEntity.OnChange((char*)(file + "\\StickFighterWalk1.bmp")
                                               .c_str(), 4);
        else
            playerEntity.OnChange((char*)(file + "\\StickFighterWalk2.bmp")
                                               .c_str(), 4);
    }
    
}

//assigns the kick animation.
void Player::kick()
{
    if(dirSwitch)
        playerEntity.OnChange((char*)(file + "\\StickFighterkick1.bmp")
                                           .c_str(), 4);
    else
        playerEntity.OnChange((char*)(file + "\\StickFighterkick2.bmp")
                                           .c_str(), 4);
}

//assigns the punch animation.
void Player::punch()
{
    if(dirSwitch)
        playerEntity.OnChange((char*)(file + "\\StickFighterPunch1.bmp")
                                           .c_str(), 4);
    else
        playerEntity.OnChange((char*)(file + "\\StickFighterPunch2.bmp")
                                           .c_str(), 4);
}

//assigns the walk animation.
void Player::walk(int d)
{
    if(dirSwitch)
        playerEntity.OnChange((char*)(file + "\\StickFighterWalk1.bmp")
                                           .c_str(), 4);
    else
        playerEntity.OnChange((char*)(file + "\\StickFighterWalk2.bmp")
                                           .c_str(), 4);
    direction = d;
    move = true;
}


void Player::stop()
{
    move = false;   
}

//moves the player across the screen.
void Player::movement()
{
    if( (clock() / 10) > (oldTime / 10)) //This prevents a bug where the movement speed scaled with speed 
    {                                   //of the processor. By limiting how many pixels it can move per clock cycle.
        if(move && direction > 0)
        {
            if(playerEntity.getX() < 480) //stops movement off screen, need to swap so not hard coded.
                ++playerEntity;
        }
        else if(move && direction < 0)
        {
            if(playerEntity.getX() > 0)
                --playerEntity;
        }
    oldTime = clock();
    }
}

//makes the player jump
void Player::jump(int max)
{
    if(isFalling == false)
    {
        playerEntity + max;
    }
}

//controls the players gravity.
void Player::falling()
{
    if(playerEntity.getY() < ground && ((clock() / 2) > (oldTime / 2)))
    {
        isFalling = true;
        playerEntity - 1;
        oldTime = clock();
    }
    else if(playerEntity.getY() == ground)
    {
        isFalling = false;
    }
}

//part of the screen drawing.
void Player::pushback()
{
    healthEntity.push();
    playerEntity.push();
}

//manages health bar.
bool Player::takeDamage(int damage)
{
    health -= damage;
    switch(health/10)
    {
        case 1:
            healthEntity.OnChange((char*)(file + "\\10.bmp").c_str(), 1);
            break;
        case 2:
            healthEntity.OnChange((char*)(file + "\\20.bmp").c_str(), 1);
            break;
        case 3:
            healthEntity.OnChange((char*)(file + "\\30.bmp").c_str(), 1);
            break;
        case 4:
            healthEntity.OnChange((char*)(file + "\\40.bmp").c_str(), 1);
            break;
        case 5:
            healthEntity.OnChange((char*)(file + "\\50.bmp").c_str(), 1);
            break;
        case 6:
            healthEntity.OnChange((char*)(file + "\\60.bmp").c_str(), 1);
            break;  
        case 7:
            healthEntity.OnChange((char*)(file + "\\70.bmp").c_str(), 1);
            break;
        case 8:
            healthEntity.OnChange((char*)(file + "\\80.bmp").c_str(), 1);
            break;
        case 9:
            healthEntity.OnChange((char*)(file + "\\90.bmp").c_str(), 1);
            break;
    }
    if(health <=0)
       return true;
    else 
        return false;
}

//damage calculation
int Player::doDamage()
{
    return playerCharacter->getPower() * 5;
}

//turns character.
void Player::turn(bool side)
{
    dirSwitch = side;   
}

int Player::getHealth()
{
    return health;    
}

void Player::setHealth(int h)
{
    health = h;
}

int Player::getX()
{
    return playerEntity.getX();    
}

void Player::OnCleanup()
{
    delete playerCharacter;
}

Player::~Player()
{
    delete playerCharacter;  
}

