#include "CApp.h"

void CApp::OnEvent(SDL_Event* Event) {
    CEvent::OnEvent(Event);
}

void CApp::OnExit() {
    Running = false;
}

//added
void CApp::OnKeyDown(SDLKey sym, SDLMod mod, Uint16 unicode) {
    
    bool gameOver = false;
    //switch based on what key is pressed
    switch(sym)
    {
        case SDLK_LCTRL:
            playerOne.kick();
            //checks if players intersect and if so does/calculates damage done.
            if(playerOne.getX() + 100 > playerTwo.getX() 
                                && playerOne.getX() < playerTwo.getX())
                gameOver = playerTwo.takeDamage(playerOne.doDamage());
            else if(playerOne.getX() - 100 < playerTwo.getX() 
                                     && playerOne.getX() > playerTwo.getX())
                gameOver = playerTwo.takeDamage(playerOne.doDamage());
            break;
        
        case SDLK_LSHIFT:
            playerOne.punch();
            //checks if players intersect and if so does/calculates damage done.
            if(playerOne.getX() + 100 > playerTwo.getX() 
                                && playerOne.getX() < playerTwo.getX())
                gameOver = playerTwo.takeDamage(playerOne.doDamage());
            else if(playerOne.getX() - 100 < playerTwo.getX() &&
                                      playerOne.getX() > playerTwo.getX())
                gameOver = playerTwo.takeDamage(playerOne.doDamage());
            break;
            
        case SDLK_d:
             //moves character
            playerOne.walk(1);
            break;
            
        case SDLK_a:
             //moves character
            playerOne.walk(-1);
            break;
            
        case SDLK_w:
             //moves character
            playerOne.jump(100);
            break;
            
        case SDLK_RCTRL:
            playerTwo.kick();
            //checks if players intersect and if so does/calculates damage done.
            if(playerOne.getX() + 100 > playerTwo.getX() 
                                && playerOne.getX() < playerTwo.getX())
                gameOver = playerOne.takeDamage(playerTwo.doDamage());
            else if(playerOne.getX() - 100 < playerTwo.getX() 
                                     && playerOne.getX() > playerTwo.getX())
                gameOver = playerOne.takeDamage(playerTwo.doDamage());
            break;
        
        case SDLK_RSHIFT:
            playerTwo.punch();
            //checks if players intersect and if so does/calculates damage done.
            if(playerOne.getX() + 100 > playerTwo.getX() 
                                && playerOne.getX() < playerTwo.getX())
                gameOver = playerOne.takeDamage(playerTwo.doDamage());
            else if(playerOne.getX() - 100 < playerTwo.getX() 
                                     && playerOne.getX() > playerTwo.getX())
                gameOver = playerOne.takeDamage(playerTwo.doDamage());
            break;
            
        case SDLK_RIGHT:
             //moves character
            playerTwo.walk(1);
            break;
            
        case SDLK_LEFT:
             //moves character
            playerTwo.walk(-1);
            break;
            
        case SDLK_UP:
             //moves character
            playerTwo.jump(100);
            break;
            
        
    }
    
    //if Game Over Reset.
    if(gameOver)
    {
        playerOne.drawCharacter(0, true);
        playerOne.setHealth(100);
    
        playerTwo.drawCharacter(340, false);
        playerTwo.setHealth(100);
        
    } 
    
 
}
//added
void CApp::OnKeyUp(SDLKey sym, SDLMod mod, Uint16 unicode) {
    switch(sym)
    {   
        case SDLK_d:
             //corrects animations
            playerOne.stop();
            playerOne.stand();
            break;
            
        case SDLK_a:
             //corrects animations
            playerOne.stop();
            playerOne.stand();
            break;
            
        case SDLK_RIGHT:
             //corrects animations
            playerTwo.stop();
            playerTwo.stand();
            break;
            
        case SDLK_LEFT:
             //corrects animations
            playerTwo.stop();
            playerTwo.stand();
            break;
            
        default:
                //corrects animations
            playerTwo.stand();
            playerOne.stand();
    }
}
 
