#include "Character.h"

Character::Character()
{
    speed = 3;
    power = 3;
    comboDamage = 3;
    comboSet = 1;
    name = "default";    
}

Character::Character(string n)
{
    speed = 3;
    power = 3;
    comboDamage = 3;
    comboSet = 1;
    name = n;    
}

Character::Character(int s, int p, int cB, int cS)
{
    speed = s;
    power = p;
    comboDamage = cB;
    comboSet = cS;       
}

void Character::setValues(int s, int p, int cB, int cS)
{
    speed = s;
    power = p;
    comboDamage = cB;
    comboSet = cS;       
}

int Character::getPower()
{
    return power;    
}

string Character::getName()
{
    return name;    
}

Character::~Character() {}
